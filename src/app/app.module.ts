import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ChatComponent } from './components/chat/chat.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChatService } from './services/chat.service';
import { AuthComponent } from './components/auth/auth.component';
import { AuthService } from './services/auth.service';
import { NavComponent } from './components/nav/nav.component';
import { ChatGroupComponent } from './components/chat-group/chat-group.component';
import { RouterModule, Routes } from '@angular/router';
import { RoomsComponent } from './components/rooms/rooms.component';
import { ChatRoomsComponent } from './components/chat-rooms/chat-rooms.component';
import { ChatRoomComponent } from './components/chat-room/chat-room.component';
import { ActiveUserService } from './services/active-user.service';
import { LoaderComponent } from './components/loader/loader.component';
import { LoaderService } from './services/loader.service';
import { UserMenuComponent } from './components/user-menu/user-menu.component';

const appRoutes: Routes = [
  { path: '',
    component: AuthComponent
  },
  {
    path: 'rooms',
    component: ChatRoomsComponent,
  },
  {
    path: 'room',
    component: ChatRoomComponent,
  }
 // { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    AuthComponent,
    NavComponent,
    ChatGroupComponent,
    RoomsComponent,
    ChatRoomsComponent,
    ChatRoomComponent,
    LoaderComponent,
    UserMenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [ChatService, AuthService, ActiveUserService, LoaderService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
