export enum AuthType {
  GOOGLE,
  APP
}

export interface AuthAction {
  username: string;
  type: AuthType;
}

export interface Message {
  username: User;
  text: string;
  time: string;
  date?: string;
}

export interface User {
  username: string
}

export enum ChatEventType {
  SEND_MESSAGE = 'send-message',
  USER_JOIN = 'user-joined',
  NEW_USER = 'new-user',
  USER_LEFT = 'user-left',
  CREATE_ROOM = 'create-room'
}
