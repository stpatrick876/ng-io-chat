import { Injectable } from '@angular/core';
import { ChatService } from './chat.service';

@Injectable()
export class AuthService {

  constructor(_chatService: ChatService) { }

  setUserName(username: string) {
    sessionStorage.setItem('username', username);
  }
  getUserName() {
    return sessionStorage.getItem('username');
  }
}
