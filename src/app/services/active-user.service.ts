import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class ActiveUserService {
  private subject = new ReplaySubject<any>();

  constructor() {
    const user = JSON.parse(sessionStorage.getItem('user'));
    if (user) {
      this.setUser(user);

    }

  }

  setUser(user: string) {
    sessionStorage.setItem('user', JSON.stringify(user))
    this.subject.next(user);
  }

  clearUser() {
    sessionStorage.removeItem('user')
    this.subject.next(null);
  }

  getUser(): Observable<any> {
    return this.subject.asObservable();
  }
}
