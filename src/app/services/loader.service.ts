import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoaderService {
  private subject = new ReplaySubject<any>();

  constructor() {}

  showLoader() {
    this.subject.next(true);
  }

  hideLoader() {
    this.subject.next(false);
  }

  getLoaderState(): Observable<any> {
    return this.subject.asObservable();
  }

}
