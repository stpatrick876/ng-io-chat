import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import SocketIO from 'socket.io-client';

@Injectable()
export class ChatService {
  private url  = 'http://localhost:8000';
  private socket: any;
  constructor() {
    this.socket =  SocketIO(this.url);

  }

  /**
   *  register user name
   * @param user
   */
  userLogin(user: any){
    this.socket.emit('user-login', user);
  }

  /**
   * get current session user
   */
  getUser() {
    return  Observable.create((observer: any) => {
      this.socket.on('user-logged-in', (data: any) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  /**
   * get a list of users in a chat rooms
   * @returns {any}
   */
  onUserUpdate() {
    return  Observable.create((observer: any) => {
      this.socket.on('user-update', (data: any) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  /**
   * get list of available chat rooms
   * @returns {any}
   */
  getRooms() {
    return  Observable.create((observer: any) => {
      this.socket.on('rooms', (data: any) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  /**
   * join a chat room
   * @param {string} room
   * @returns {any}
   */
  joinRoom(room: string) {
    this.socket.emit('join-room', room);
    return  Observable.create((observer: any) => {
      this.socket.on('room', (data: any) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });

  }

  /**
   * sens message to a chat room
   * @param {string} message
   * @param {string} username
   */
  sendMessage(message: string) {
    this.socket.emit('add-message', message);
  }

  /**
   * get messages for a chat room
   * @returns {any}
   */
  getMessages() {
    return  Observable.create((observer: any) => {
      this.socket.on('message', (data: any) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }

  /**
   *
   */
  fetchRooms() {
    this.socket.emit('re-push-rooms');
  }

  typing() {
    this.socket.emit('typing');

  }
  /**
   * get messages for a chat room
   * @returns {any}
   */
  getTypingUsers() {
    return  Observable.create((observer: any) => {
      this.socket.on('is-typing', (data: any) => {
        observer.next(data);
      });
      return () => {
        this.socket.disconnect();
      };
    });
  }
  updateUser(updatedUser: any) {
    this.socket.emit('update-user', updatedUser);

  }
}
