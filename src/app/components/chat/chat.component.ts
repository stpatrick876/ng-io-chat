import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { ActiveUserService } from '../../services/active-user.service';

@Component({
  selector: 'ng-io-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  message: string;
  @Input() messages: any[];
  user: any;
  @Output() sendMessage: EventEmitter<string> = new EventEmitter<string>();
  typingUsers: any[] = [];
  constructor(private _activeUser: ActiveUserService, private _chatService: ChatService) { }

  ngOnInit() {
   this._activeUser.getUser().subscribe(user => {
     this.user = user;
     console.log('user in chat is ', user)
   });

/*   this._chatService.getTypingUsers().subscribe(username => {
      this.typingUsers.push(username) ;
     console.log('res typing', username)
   });*/
  }


  newMessage() {
    this.sendMessage.emit(this.message)
    this.message = '';
  }

  typing() {
   // this._chatService.typing();
  }
}
