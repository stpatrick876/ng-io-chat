import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActiveUserService } from '../../services/active-user.service';
import { ChatService } from '../../services/chat.service';
declare var $: any;
@Component({
  selector: 'ng-io-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, AfterViewInit {
   user: any;
  constructor(private _router: Router, public _userService: ActiveUserService, private _chatService: ChatService) {

  }

  ngOnInit() {
    this._userService.getUser().subscribe(user => {
      this.user = user;
    });
  }

  ngAfterViewInit() {
    $(document).ready(function(){
      $('.button').click(function () {
        $(this).parent().toggleClass('expand');
      });
    });


  }

  leaveApp() {
    this._router.navigate(['/']).then(() => {
      this._userService.clearUser();

    });
  }

  goToList() {
    // TODO: leave room confirmation an server implimentations
    this._router.navigate(['rooms'])
  }

}
