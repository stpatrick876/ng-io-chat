import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isNullOrUndefined } from 'util';
import { ChatService } from '../../services/chat.service';
import { ActiveUserService } from '../../services/active-user.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import 'rxjs/add/operator/filter';

declare var $: any;
declare var Materialize: any;
@Component({
  selector: 'ng-io-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  username: string;
  userMenuForm: FormGroup;

  constructor(private _fb: FormBuilder, private _chatService: ChatService, private _activeUserService: ActiveUserService, private _router: Router) { }

  ngOnInit() {

    this.userMenuForm = this._fb.group({
      username: ['', Validators.required],

    });

     this._activeUserService.getUser().subscribe(user => {
       if(!isNullOrUndefined(user)) {
         this.userMenuForm.controls['username'].setValue(user.username.slice(1));

       }
     });



    this._chatService.onUserUpdate().subscribe(res => {
      if(!res) {
        return false;
      }
      this._activeUserService.setUser(res.user);
      Materialize.toast(`Handle updated to ${res.user.username}`, 4000, 'success-toast') // 4000 is the duration of the toast
      $('#userMenuContainer').removeClass('expand');
    });

    this._router.events.filter(event => event instanceof NavigationEnd)
      .subscribe((event: any) => {

        if(event.url.split('name')[0] === '/room;') {
          this.userMenuForm.controls['username'].disable();
        } else {
          this.userMenuForm.controls['username'].enable();
        }
      });
  }

  updateUser(formVal: any) {
    console.log('form val is', formVal)
    const updatedName = `@${formVal.username}`;

    this._chatService.updateUser({username: updatedName});


  }

}
