import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { Router } from '@angular/router';
import { ActiveUserService } from '../../services/active-user.service';
import { LoaderComponent } from '../loader/loader.component';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'ng-io-chat-rooms',
  templateUrl: './chat-rooms.component.html',
  styleUrls: ['./chat-rooms.component.scss']
})
export class ChatRoomsComponent implements OnInit {
  roomName: string;
  rooms: any[];
  user: any;
  isLoading = true;
  constructor(private _chatService: ChatService, private _router: Router, private _activeUser: ActiveUserService) { }

  ngOnInit() {
    this._activeUser.getUser().subscribe(user => {
      this.user = user;
    });

    // TODO: find better solution
    this._chatService.fetchRooms();


    this._chatService.getRooms().subscribe(res => {
      this.rooms = res.rooms;
      setTimeout(() => {
        this.isLoading = false;
      }, 1000);
    });


  }


  joinRoom(room: string) {
    this._router.navigate(['/room', {name: room}]);
  }

}
