import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthAction, AuthType } from '../../common.interface';
import { AuthService } from '../../services/auth.service';
import { ChatService } from '../../services/chat.service';
import { Router } from '@angular/router';
import { ActiveUserService } from '../../services/active-user.service';

@Component({
  selector: 'ng-io-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  usernameInput: string;
  authenticated = false;
  @Output() authActionEvent: EventEmitter<AuthAction> = new EventEmitter<AuthAction>();

  constructor(private _authService: AuthService, private _chatService: ChatService, private _router: Router, private _activeUserService: ActiveUserService) { }

  ngOnInit() {
   this._chatService.getUser().subscribe(res => {
     this._activeUserService.setUser(res.user);
     this._router.navigate(['rooms']);

   });
  }

  get username() {
    return `@${this.usernameInput}`;
  }



  authenticationAction() {
    const e =  {
      username: this.username,
      type: AuthType.APP
    };

   //  this._authService.setUserName(JSON.stringify(e));
   // this.authActionEvent.emit(e);
   // this.authenticated = true;
    this._chatService.userLogin({username: this.username});
  }
}
