import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ng-io-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.scss']
})
export class ChatRoomComponent implements OnInit, OnDestroy {
  room: any;
  messages: any[] = [];
  isLoading = true;
  private sub: any;

  constructor(private _chatService: ChatService, private _cdr: ChangeDetectorRef, private _route: ActivatedRoute) { }

  ngOnInit() {
    setTimeout(() => {
      this.isLoading = false;
    }, 1000);
    this._chatService.getMessages().subscribe(message => {
      this.messages.push(message);
      console.log('messages got', this.messages)
    });
    this.sub = this._route.params.subscribe(params => {
     const room = params['name']; // (+) converts string 'id' to a number

      this._chatService.joinRoom(room)
            .subscribe(res => {
              this.room = res.room;
            });
    });

  }

  onSendMessage(e: any) {
    this._chatService.sendMessage(e);
  }


  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
