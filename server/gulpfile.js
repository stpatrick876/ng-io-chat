/**
 * Created by kebell on 1/17/2018.
 */
var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var nodemon = require('gulp-nodemon');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');

gulp.task("build", function () {
  return tsProject.src()
    .pipe(tsProject())
    .js.pipe(gulp.dest("./dist"));
});

// Task
gulp.task('default', ['build'], function() {
  // listen for changes
  livereload.listen();
  // configure nodemon
  nodemon({
    // the script to run the app
    script: './dist/index.js',
    ext: 'ts',
    tasks: ['build']

  }).on('restart', function(){
    // when the app has restarted, run livereload.
    gulp.src('./dist/index.js')
      .pipe(livereload());
      //.pipe(notify('Reloading page, please wait...'));
  })
})
