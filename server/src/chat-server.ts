/**
 * Created by kebell on 1/17/2018.
 */
import * as express from 'express';
import * as socketIo from 'socket.io';
import { createServer, Server } from 'http';
import {Utils} from "./utils";
import { FirestoreRepo } from './firestoreRepo';
import { ChatSession } from './models/ChatSession';
import { SessionManagement } from './sessionManagement';

export class ChatServer {
  public static readonly PORT:number = 8000;
  private app: express.Application;
  private server: Server;
  private io: SocketIO.Server;
  private port: string | number;
  sessionManagement = new SessionManagement();
  db = new FirestoreRepo().init();


  constructor() {
    this.createApp();
    this.config();
    this.createServer();
    this.sockets();
    this.listen();
  }

  private createApp(): void {
    this.app = express();
  }

  private createServer(): void {
    this.server = createServer(this.app);
  }
  private config(): void {
    this.port = process.env.PORT || ChatServer.PORT;
  }
  private sockets(): void {
    this.io = socketIo(this.server);
  }

  /**
   * read collection of rooms from firebase and emit event with rooms
   */
  private initRooms() {
     this.db.collection('rooms').get()
       .then((snapshots) => {
         const rooms: any[] = [];
         snapshots.forEach((doc) => {
           rooms.push(doc.data());
         });

         this.io.emit('rooms', {type: 'rooms', rooms: rooms});

       })
       .catch((err) => {
         console.log('Error getting documents', err);
       });

  }

  private listen(): void {
    this.server.listen(this.port, () => {
      console.log('Running server on port %s', this.port);
    });

    /**
     * Listener: socket io user connection event
     */
    this.io.on('connection', (socket) => {
      console.log('User Connected');
      const session: any = {};

      session.sessionId = socket.id;


      this.initRooms();


      /**
       * Listener: socket io user disconnection event
       */
      socket.on('disconnect',  () => {
        console.log('User Disconected')
      });


      /**
       * Listener: socket io new message event
       */
      socket.on('add-message',  (message) => {
        console.log('message is ', message)
        this.io.emit('message', {type: 'new-message', text: message, username: session.username, time: Utils.format_time(new Date())});
      });


      /**
       * Listener: socket io new user sign in
       */
      socket.on('user-login',  (data) => {
        console.log('user-login', data.username)
        // store the username in the session for this client
       session.username = data.username;
       session.userId = Utils.generateId();


        this.sessionManagement.add(session)

        socket.emit('user-logged-in', {type: 'new user', user: {username: session.username, userId: session.userId}});
        this.initRooms();

      });


      /**
       * Listener: socket io new user sign in
       */
      socket.on('join-room',  (room) => {

       // console.log('joining room ', room, this.sessionManagement.sessions)
        socket.join(room);
        session.room = room;
        console.log('use ',  session)
        socket.to(room).emit('message', {type: 'room-notification', text: `user ${session.username} has joined room`, time: Utils.format_time(new Date())});
        socket.emit('message', {type: 'user-notification', text: `hello ${session.username}, welcome to the room`, time: Utils.format_time(new Date())});

        this.db.collection('rooms').where('name', '==', room)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              this.io.emit('room', {type: 'rooms', room:  doc.data(), time: Utils.format_time(new Date())});

            });
          })
          .catch((error) => {
            console.log('Error getting documents: ', error);
          });



      });

      /**
       *  re-push rooms
       */

      socket.on('re-push-rooms', () => {
        console.log('re-push called')
        this.initRooms();
      });

      /**
       *  re-push rooms
       */

      socket.on('typing', () => {
        console.log('re-push called')
        socket.to(session.room).emit('is-typing', session.username);
      });

      /**
       * update current user
       */

      socket.on('update-user', (data) => {
        console.log('updating user', data)
        session.username = data.username;
        socket.emit('user-update', {type: 'updated user', user: {username: session.username, userId: session.userId}});
      });

      /** End of Connection**/
    });

  }

  public getApp(): express.Application {
    return this.app;
  }
}
