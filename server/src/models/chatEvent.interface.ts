import {User} from "./User";
/**
 * Created by kebell on 1/17/2018.
 */


export interface ChatEventPayload {
  type: string;
  content: any;
  time?: string;
  user?: User;
  additional?: any;
}
