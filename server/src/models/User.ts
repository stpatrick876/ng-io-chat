import {Utils} from "../utils";
/**
 * Created by kebell on 1/17/2018.
 */
export class User {
  private _id: string;
  constructor(private username: string) {
    this._id  = Utils.generateId();
  }

  public get id(){
    return this._id;
  }

}
