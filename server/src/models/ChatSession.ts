export interface ChatSession {
  sessionId: string;
  username: string;
  userId: string;
  loggedInTime?: string;
}
