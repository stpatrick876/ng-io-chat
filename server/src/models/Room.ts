import {User} from "./User";
/**
 * Created by kebell on 1/17/2018.
 */
export class Room {
  constructor(private name: string, private created_by: User, private created_at: string) {}
}
