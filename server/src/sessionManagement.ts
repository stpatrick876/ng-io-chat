export class SessionManagement {
   sessions: any[] = [];

    // User roles list
   userRoles: any = {
    Admin: 'administrator',
    User: 'user',
  };

  /**
   * find a session by id
   * @param sessionId
   * @returns {any}
   */
  indexOf(sessionId: string): number | null {

    this.sessions.forEach((session, i) => {
      if (session.sessionId === sessionId){
        return i;
      }
    });
    return null;
  }

  /**
   * find a user in session
   * @param userId
   * @returns {any}
   */
  indexOfUser(userId: string): number | null {
    this.sessions.forEach((session, i) => {
      if (session.userId === userId){
        return i;
      }
    });
    return null;
  }

  /**
   * add data to session
   * @param sessionData
   */
  add(sessionData: any) {
    this.sessions.push(sessionData);
  }

  /**
   * remove session object by session id
   * @param sessionId
   * @returns {any}
   */
  remove(sessionId: string): any  {
    const index = this.indexOf(sessionId);
    if (index != null) {
      this.sessions.splice(index, 1);
    } else {
      return null;
    }
  }

  /**
   * remove session object by user id
   * @param {string} userId
   * @returns {any}
   */
  removeByUserId(userId: string): any {
    const index = this.indexOf(userId);
    if (index != null) {
      this.sessions.splice(index, 1);
    } else {
      return null;
    }
  }

  /**
   * get a session by Id
   * @param {string} userId
   * @returns {any}
   */
  getSessionById(userId: string) {
    const index = this.indexOfUser(userId);
    if (index != null) {
      return this.sessions[index];
    } else {
      return null;
    }
  }

  /**
   * get a session b y userId
   * @param {string} userId
   * @returns {any}
   */
  getSessionByUserId(userId: string) {
    const index = this.indexOfUser(userId);
    if (index != null) {
      return this.sessions[index];
    } else {
      return null;
    }
  }








}
