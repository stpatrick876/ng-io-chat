/**
 * Created by kebell on 1/17/2018.
 */
export class Utils {
  static format_time(date_obj: any) {
    // formats a javascript Date object into a 12h AM/PM time string
    let hour = date_obj.getHours();
    let minute = date_obj.getMinutes();
    const amPM = (hour > 11) ? 'pm' : 'am';
    if(hour > 12) {
      hour -= 12;
    } else if(hour === 0) {
      hour = '12';
    }
    if(minute < 10) {
      minute = '0' + minute;
    }
    return hour + ':' + minute + amPM;
  }

  static generateId() {
    return '_' + Math.random().toString(36).substr(2, 9);
  };

}
