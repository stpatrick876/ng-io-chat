import * as admin from 'firebase-admin'

export class FirestoreRepo {

  init() {
    const serviceAccount = require(require('path').resolve(__dirname, '../hobby chat-39bf094a420d.json'));

    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: 'https://hobby-chat.firebaseio.com'
    });

    return admin.firestore();
  }
}
