"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by kebell on 1/17/2018.
 */
var express = require("express");
var socketIo = require("socket.io");
var http_1 = require("http");
var utils_1 = require("./utils");
var firestoreRepo_1 = require("./firestoreRepo");
var sessionManagement_1 = require("./sessionManagement");
var ChatServer = /** @class */ (function () {
    function ChatServer() {
        this.sessionManagement = new sessionManagement_1.SessionManagement();
        this.db = new firestoreRepo_1.FirestoreRepo().init();
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }
    ChatServer.prototype.createApp = function () {
        this.app = express();
    };
    ChatServer.prototype.createServer = function () {
        this.server = http_1.createServer(this.app);
    };
    ChatServer.prototype.config = function () {
        this.port = process.env.PORT || ChatServer.PORT;
    };
    ChatServer.prototype.sockets = function () {
        this.io = socketIo(this.server);
    };
    /**
     * read collection of rooms from firebase and emit event with rooms
     */
    ChatServer.prototype.initRooms = function () {
        var _this = this;
        this.db.collection('rooms').get()
            .then(function (snapshots) {
            var rooms = [];
            snapshots.forEach(function (doc) {
                rooms.push(doc.data());
            });
            _this.io.emit('rooms', { type: 'rooms', rooms: rooms });
        })
            .catch(function (err) {
            console.log('Error getting documents', err);
        });
    };
    ChatServer.prototype.listen = function () {
        var _this = this;
        this.server.listen(this.port, function () {
            console.log('Running server on port %s', _this.port);
        });
        /**
         * Listener: socket io user connection event
         */
        this.io.on('connection', function (socket) {
            console.log('User Connected');
            var session = {};
            session.sessionId = socket.id;
            _this.initRooms();
            /**
             * Listener: socket io user disconnection event
             */
            socket.on('disconnect', function () {
                console.log('User Disconected');
            });
            /**
             * Listener: socket io new message event
             */
            socket.on('add-message', function (message) {
                console.log('message is ', message);
                _this.io.emit('message', { type: 'new-message', text: message, username: session.username, time: utils_1.Utils.format_time(new Date()) });
            });
            /**
             * Listener: socket io new user sign in
             */
            socket.on('user-login', function (data) {
                console.log('user-login', data.username);
                // store the username in the session for this client
                session.username = data.username;
                session.userId = utils_1.Utils.generateId();
                _this.sessionManagement.add(session);
                socket.emit('user-logged-in', { type: 'new user', user: { username: session.username, userId: session.userId } });
                _this.initRooms();
            });
            /**
             * Listener: socket io new user sign in
             */
            socket.on('join-room', function (room) {
                // console.log('joining room ', room, this.sessionManagement.sessions)
                socket.join(room);
                session.room = room;
                console.log('use ', session);
                socket.to(room).emit('message', { type: 'room-notification', text: "user " + session.username + " has joined room", time: utils_1.Utils.format_time(new Date()) });
                socket.emit('message', { type: 'user-notification', text: "hello " + session.username + ", welcome to the room", time: utils_1.Utils.format_time(new Date()) });
                _this.db.collection('rooms').where('name', '==', room)
                    .get()
                    .then(function (querySnapshot) {
                    querySnapshot.forEach(function (doc) {
                        _this.io.emit('room', { type: 'rooms', room: doc.data(), time: utils_1.Utils.format_time(new Date()) });
                    });
                })
                    .catch(function (error) {
                    console.log('Error getting documents: ', error);
                });
            });
            /**
             *  re-push rooms
             */
            socket.on('re-push-rooms', function () {
                console.log('re-push called');
                _this.initRooms();
            });
            /**
             *  re-push rooms
             */
            socket.on('typing', function () {
                console.log('re-push called');
                socket.to(session.room).emit('is-typing', session.username);
            });
            /**
             * update current user
             */
            socket.on('update-user', function (data) {
                console.log('updating user', data);
                session.username = data.username;
                socket.emit('user-update', { type: 'updated user', user: { username: session.username, userId: session.userId } });
            });
            /** End of Connection**/
        });
    };
    ChatServer.prototype.getApp = function () {
        return this.app;
    };
    ChatServer.PORT = 8000;
    return ChatServer;
}());
exports.ChatServer = ChatServer;
