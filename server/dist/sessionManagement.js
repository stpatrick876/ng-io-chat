"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SessionManagement = /** @class */ (function () {
    function SessionManagement() {
        this.sessions = [];
        // User roles list
        this.userRoles = {
            Admin: 'administrator',
            User: 'user',
        };
    }
    /**
     * find a session by id
     * @param sessionId
     * @returns {any}
     */
    SessionManagement.prototype.indexOf = function (sessionId) {
        this.sessions.forEach(function (session, i) {
            if (session.sessionId === sessionId) {
                return i;
            }
        });
        return null;
    };
    /**
     * find a user in session
     * @param userId
     * @returns {any}
     */
    SessionManagement.prototype.indexOfUser = function (userId) {
        this.sessions.forEach(function (session, i) {
            if (session.userId === userId) {
                return i;
            }
        });
        return null;
    };
    /**
     * add data to session
     * @param sessionData
     */
    SessionManagement.prototype.add = function (sessionData) {
        this.sessions.push(sessionData);
    };
    /**
     * remove session object by session id
     * @param sessionId
     * @returns {any}
     */
    SessionManagement.prototype.remove = function (sessionId) {
        var index = this.indexOf(sessionId);
        if (index != null) {
            this.sessions.splice(index, 1);
        }
        else {
            return null;
        }
    };
    /**
     * remove session object by user id
     * @param {string} userId
     * @returns {any}
     */
    SessionManagement.prototype.removeByUserId = function (userId) {
        var index = this.indexOf(userId);
        if (index != null) {
            this.sessions.splice(index, 1);
        }
        else {
            return null;
        }
    };
    /**
     * get a session by Id
     * @param {string} userId
     * @returns {any}
     */
    SessionManagement.prototype.getSessionById = function (userId) {
        var index = this.indexOfUser(userId);
        if (index != null) {
            return this.sessions[index];
        }
        else {
            return null;
        }
    };
    /**
     * get a session b y userId
     * @param {string} userId
     * @returns {any}
     */
    SessionManagement.prototype.getSessionByUserId = function (userId) {
        var index = this.indexOfUser(userId);
        if (index != null) {
            return this.sessions[index];
        }
        else {
            return null;
        }
    };
    return SessionManagement;
}());
exports.SessionManagement = SessionManagement;
