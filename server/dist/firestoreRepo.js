"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var admin = require("firebase-admin");
var FirestoreRepo = /** @class */ (function () {
    function FirestoreRepo() {
    }
    FirestoreRepo.prototype.init = function () {
        var serviceAccount = require(require('path').resolve(__dirname, '../hobby chat-39bf094a420d.json'));
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: 'https://hobby-chat.firebaseio.com'
        });
        return admin.firestore();
    };
    return FirestoreRepo;
}());
exports.FirestoreRepo = FirestoreRepo;
