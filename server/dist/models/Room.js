"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by kebell on 1/17/2018.
 */
var Room = /** @class */ (function () {
    function Room(name, created_by, created_at) {
        this.name = name;
        this.created_by = created_by;
        this.created_at = created_at;
    }
    return Room;
}());
exports.Room = Room;
