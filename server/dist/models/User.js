"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../utils");
/**
 * Created by kebell on 1/17/2018.
 */
var User = /** @class */ (function () {
    function User(username) {
        this.username = username;
        this._id = utils_1.Utils.generateId();
    }
    Object.defineProperty(User.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
